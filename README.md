## Pixel ##
Esialgne idee oli koostada 'platformer'-mäng, kus mängija on väike ruut kes liigub väljakul ringi.
Graafika on minimalistlik, kõik koosneb põhiliselt ruutudest, kuid põhieesmärgiks oli mõelda välja viis,
kuidas muusikaga rütmi panna potentsiaalsed ohud ja võib-olla ka tavalised liikuvad platvormid.
Hetkel on veel vaja mõelda, kuidas mängija 'respawnib' iga surmaga ja luua mingid heliefektid, mida mängitakse 
(heliefektid on eraldi programmiga loodud)